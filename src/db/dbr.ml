open Misc_db
open Db_lwt
open MLet

let get_accounts () =
  let>>> dbh = () in
  let>! r = [%pgsql dbh "select pkh, folding_name, folding_id, last_tsp, last_credit from account"] in
  List.map donor_of_db r

let get_last_cycle () =
  let>>> dbh = () in
  let>! r = [%pgsql dbh "select last_cycle from state"] in
  MRes.one ~empty:"No state stored" (fun s -> s) r

let get_account id =
  let id_int = Int64.of_string_opt id in
  let>>> dbh = () in
  let>! r = [%pgsql dbh
      "select pkh, folding_name, folding_id, last_tsp, last_credit from account \
       where pkh = $id or folding_name = $id or folding_id = $?id_int"] in
  MRes.one ~empty:"no donor with this id" donor_of_db r

let get_distribution ?cycle () =
  let@ cycle = match cycle with
    | None -> get_last_cycle ()
    | Some cycle -> Lwt.return (Ok cycle) in
  let>>> dbh = () in
  let> r = [%pgsql dbh
      "select hash, amount, cycle, ratio from distributions where cycle = $cycle"] in
  let@ distrib = Lwt.return @@ MRes.one ~empty:"no distribution for this cycle" (fun x -> x) r in
  let>+ trs = [%pgsql dbh "select pkh, amount from transactions where cycle = $cycle"] in
  distrib, trs
