(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Dune_types_min
open MRes
open MLwt

module Encoding = struct
  open Json_encoding
  open Dune_encoding_min

  let int64 = EzEncoding.int64
  let z_encoding = z_encoding

  let forge_ops =
    obj2
      (req "branch" string)
      (req "contents" Operation.contents_and_result_list_encoding)

  let preapply_ops =
    list (obj4
            (opt "branch" string)
            (req "contents" Operation.contents_and_result_list_encoding)
            (opt "protocol" string)
            (opt "signature" string))

  let run_operation_input =
    obj2
      (req "operation"
         (obj3
            (req "branch" string)
            (req "contents" Operation.contents_and_result_list_encoding)
            (req "signature" string)))
      (req "chain_id" string)

  let run_operation_output =
    obj2
      (req "contents" Operation.contents_and_result_list_encoding)
      (opt "signature" string)

  let entrypoints =
    obj1 (req "entrypoints" (assoc Script.expr_encoding))
end

module Services = struct

  type nonrec 'a service0 = ('a, MRes.error, EzAPI.no_security) EzAPI.service0
  type nonrec ('a, 'b) service1 = ('a, 'b, MRes.error, EzAPI.no_security) EzAPI.service1
  type nonrec ('a, 'b) post_service0 = ('a, 'b, MRes.error, EzAPI.no_security) EzAPI.post_service0

  open Dune_encoding_min

  let arg_hash = EzAPI.arg_string "hash" "dn1MLnf3qjGsnaStSg1jMmsdgXKz9hteWE9i"

  let blocks_path = EzAPI.Path.(root // "chains" // "main" // "blocks")
  let head_path = EzAPI.Path.(blocks_path // "head")
  let ctxt_path = EzAPI.Path.(head_path // "context")

  let account_info : (string, node_account_info) service1 =
    EzAPI.service
      ~output:Account.account_info
      EzAPI.Path.(ctxt_path // "contracts" /: arg_hash // "info")

  let header : (string, block_header) service1 =
    EzAPI.service
      ~output:Header.encoding
      EzAPI.Path.(blocks_path /: arg_hash // "header")

  let constants : constants service0 =
    EzAPI.service
      ~output:Constants.encoding
      EzAPI.Path.(ctxt_path // "constants")

  let contract_service output path =
    EzAPI.service
      ~output
      EzAPI.Path.(ctxt_path // "contracts" // path)

  let manager_key : (string, string option) service1 =
    EzAPI.service
      ~output:Json_encoding.(option string)
      EzAPI.Path.(ctxt_path // "contracts" /: arg_hash // "manager_key")

  let entrypoints : (string, (string * script_expr_t) list) service1 =
    EzAPI.service
      ~output:Encoding.entrypoints
      EzAPI.Path.(ctxt_path // "contracts" /: arg_hash // "entrypoints")

  let entrypoint_type : (string, script_expr_t) service1 =
    EzAPI.service
      ~output:Script.expr_encoding
      EzAPI.Path.(ctxt_path // "contracts" /: arg_hash)

  type operation = string option * node_operation_type list * string option * string option
  let preapply : (operation list, operation list) post_service0 =
    EzAPI.post_service
      ~input:Encoding.preapply_ops
      ~output:Encoding.preapply_ops
      EzAPI.Path.(head_path // "helpers/preapply/operations")

  let run_operation : ((string * node_operation_type list* string) * string,
                       node_operation_type list * string option) post_service0 =
    EzAPI.post_service
      ~input:Encoding.run_operation_input
      ~output:Encoding.run_operation_output
      EzAPI.Path.(head_path // "helpers/scripts/run_operation")

end

module Make(S : EzRequest_lwt.S) = struct

  let raw_wrap_res = function
    | Error e -> return @@ Error (Gen_err e)
    | Ok x -> return @@ Ok x
  let wrap_res = function
    | Error (EzRequest_lwt.UnknwownError {code; msg}) -> return @@ Error (Gen_err (code, msg))
    | Error (EzRequest_lwt.KnownError {error; _}) -> return @@ Error error
    | Ok x -> return @@ Ok x
  let raw_destruct s = String.sub s 1 (String.length s - 3)
  let raw_construct s = "\"" ^ s ^ "\""
  let mk_url ~base:(EzAPI.TYPES.BASE base) url =
    let url =
      if String.get base (String.length base - 1) = '/' then base ^ url
      else base ^ "/" ^ url in
    EzAPI.TYPES.URL url

  (** Get *)
  let raw_get ~base ?msg url =
    S.get ?msg (mk_url ~base url) >>= raw_wrap_res >>|? raw_destruct

  let get0 ?post ?headers ?params ?msg base service =
    S.get0 ?post ?headers ?params ?msg base service >>= wrap_res
  let get1 ?post ?headers ?params ?msg base service arg =
    S.get1 ?post ?headers ?params ?msg base service arg >>= wrap_res

  let get_account_info ~base hash = get1 base Services.account_info hash
  let get_header ~base ?(block="head") () = get1 base Services.header block
  let get_head_hash ~base () =
    raw_get ~base ~msg:"head_hash" "chains/main/blocks/head/hash"
  let get_constants ~base () = get0 base Services.constants
  let get_manager_key ~base hash = get1 base Services.manager_key hash
  let get_entrypoints ~base hash = get1 base Services.entrypoints hash
  let get_entrypoint_type ~base hash ep =
    get0 base
      (Services.contract_service Dune_encoding_min.Script.expr_encoding
         (hash ^ "/entrypoints/" ^ ep))

  (** Post *)
  let raw_post ?content ?content_type ?msg ~base url =
    S.post ?content ?content_type ?msg (mk_url ~base url) >>= raw_wrap_res

  let post0 ?headers ?params ?msg ~input base service =
    S.post0 ?headers ?params ?msg ~input base service >>= wrap_res

  let preapply ~base head protocol signature ops =
    post0 ~input:[Some head, ops, Some protocol, Some signature]
      base Services.preapply

  let dummy_sign = "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQ\
                    rUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"

  let run_operation ~base ~head ~chain_id ops =
    post0 ~input:((head, ops, dummy_sign), chain_id)
      base Services.run_operation
    >>|? fun (ops, _) -> ops

  let inject ~base bytes =
    let expected_hash = Crypto.Operation_hash.hash bytes in
    raw_post ~base ~content:(raw_construct @@ Forge.to_hex bytes)
      "injection/operation" >>|? raw_destruct >>=? fun h ->
    let hash = Crypto.Operation_hash.b58dec h in
    if not (Crypto.Operation_hash.equal hash expected_hash) then
      return (Error (Str_err "Unexpected operation hash"))
    else return (Ok hash)

  let remote_forge ~base ~head ops =
    raw_post ~base
      ~content:(EzEncoding.construct Encoding.forge_ops (head, ops))
      (Misc.spf "chains/main/blocks/%s/helpers/forge/operations" head)
    >>|? raw_destruct >>|? Forge.of_hex

  (** Utils *)

  type op_more = {
    op : node_operation_type;
    bytes : Bigstring.t;
    size : int;
    size_limits : int;
    limits0 : (int64 option * Z.t option * Z.t option)
  }

  let make_op_more op limits0 =
    let (f, g, s) = Dune_utils.limits_of_operation_exn op in
    let size_limits = Binary_size.n_int64 f + Binary_size.n_zarith g + Binary_size.n_zarith s in
    {op; size_limits; size = 0; bytes = Bigstring.empty; limits0}

  let update_op_more_limits ?fee ?gas_limit ?storage_limit op_more =
    let op, diff_size =
      Dune_utils.update_limits ?fee ?gas_limit ?storage_limit op_more.op in
    {op_more with op; size = op_more.size + diff_size;
                  size_limits = op_more.size_limits + diff_size}

  let update_op_more_counters counter ops_more =
    let ops = Dune_utils.update_counters counter (List.map (fun {op; _} -> op) ops_more) in
    List.map2 (fun op op_more -> {op_more with op}) ops ops_more

  let update_op_more_bytes bytes op_more =
    let size = Bigstring.length bytes in
    {op_more with bytes; size}

  let get_op_more {op;_} = op
  let get_size_more {size; _} = size
  let get_op_bytes_more {bytes; _} = bytes
  let get_ops_more ops = List.map get_op_more ops
  let get_sizes_more ops = List.map get_size_more ops
  let get_ops_bytes_more ops = List.map get_op_bytes_more ops

  let compare_with_remote ~base ~head ops local_bytes =
    remote_forge ~base ~head ops >>=? fun remote_bytes ->
    if not @@ Bigstring.equal local_bytes remote_bytes then
      return (Error (Str_err (
          Misc.spf "Mismatch in forge operations\nlocal  %s\nremote %s"
            (Forge.to_hex local_bytes) (Forge.to_hex remote_bytes)
        )))
    else
      return (Ok local_bytes)

  let sign_operation ~sign bytes =
    sign bytes >>|? fun signature ->
    Binary_writer.list [bytes; signature]

  let sign_zero_operation bytes =
    return (Ok (Binary_writer.list [bytes; Bigstring.make 64 '\000']))

  let forge_unsigned_operation ~base ops =
    get_header ~base () >>=? fun header ->
    let head = Misc.unopt_exn header.header_hash in
    return (Forge.forge_operations head ops) >>=? fun local_bytes ->
    compare_with_remote ~base ~head ops local_bytes >>|? fun bytes ->
    bytes, ops

  let forge_activation ~base ~pkh node_act_secret =
    let act = { node_act_pkh = pkh; node_act_secret; node_act_metadata = None} in
    forge_unsigned_operation ~base [ NActivation act ]

  let minimal_fees = Z.of_int 100
  let nanodun_per_gas_unit = Z.of_int 100
  let nanodun_per_byte = Z.of_int 1000
  let to_nanodun m = Z.mul (Z.of_int 1000) m
  let of_nanodun n = Z.div (Z.add (Z.of_int 999) n) (Z.of_int 1000)

  let compute_fees ~gas_limit ~size =
    let minimal_fees_in_nanodun = to_nanodun minimal_fees in
    let fees_for_gas_in_nanodun =
      Z.mul nanodun_per_gas_unit gas_limit in
    let fees_for_size_in_nanodun = Z.mul nanodun_per_byte (Z.of_int size) in
    let fees_in_nanodun =
      Z.add minimal_fees_in_nanodun @@
      Z.add fees_for_gas_in_nanodun fees_for_size_in_nanodun in
    of_nanodun fees_in_nanodun

  let iter_compute_fees ?(first=false) op n =
    let (fee_start, gas_limit, _) = Dune_utils.limits_of_operation_exn (get_op_more op) in
    let fee_start = Z.of_int64 fee_start in
    let size_start = get_size_more op + (if first then 96 else 0) in
    let diff_size fee = Binary_size.n_zarith fee - Binary_size.n_zarith fee_start in
    let rec aux size i =
      if i = 0 then compute_fees ~gas_limit ~size
      else
        let fee = compute_fees ~gas_limit ~size in
        let diff_size = diff_size fee in
        let size = size_start + diff_size in
        aux size (i-1) in
    let fee = Z.to_int64 @@ aux size_start n in
    update_op_more_limits ~fee op

  let check_original_limits ?fee ?gas_limit ?storage_limit op =
    let fee0, gas0, storage0 = op.limits0 in
    (match fee, fee0 with
     | None, _ -> Ok None
     | fee, None -> Ok fee
     | Some fee, Some fee0 when fee0 >= fee -> Ok (Some fee0)
     | Some fee, Some _ ->
       Error (Str_err (
           Misc.spf "Fee too low, operation will never be included (minimum %Ld mudun)" fee)))
    >>? fun fee ->
    (match gas_limit, gas0 with
     | None, _ -> Ok None
     | gas, None -> Ok gas
     | Some gas, Some gas0 when gas0 >= gas -> Ok (Some gas0)
     | Some gas, Some _ ->
       Error (Str_err (
           Misc.spf "Gas limit low, operation will fail (minimum %s mudun)" (Z.to_string gas))))
    >>? fun gas_limit ->
    (match storage_limit, storage0 with
     | None, _ -> Ok None
     | storage, None -> Ok storage
     | Some storage, Some storage0 when storage0 >= storage -> Ok (Some storage0)
     | Some storage, Some _ ->
       Error (Str_err (
           Misc.spf "Storage limit low, operation will fail (minimum %s mudun)" (Z.to_string storage))))
    >|? fun storage_limit ->
    update_op_more_limits ?fee ?gas_limit ?storage_limit op

  let update_gas_storage_fee ?(first=false) op res =
    let is_reveal = match res with NReveal _ -> true | _ -> false in
    let rec aux res =
      match Dune_utils.get_manager_metadata res with
      | None | Some { manager_meta_operation_result = None ; _ } ->
        Error (Str_err "No metadata")
      | Some { manager_meta_operation_result =
                 Some ({ meta_op_status = Some "applied" ; _ } as op_res) ;
               manager_meta_internal_operation_results = op_internals ; _ } ->
        let consumed_gas = Z.(
            add (of_int (if is_reveal then 0 else 100))
              op_res.meta_op_consumed_gas) in
        let consumed_storage = op_res.meta_op_paid_storage_size_diff in
        let allocated = if op_res.meta_op_allocated_destination_contract then 1 else 0 in
        let allocated = allocated + List.length op_res.meta_op_originated_contracts in
        let consumed_storage =
          Z.add (Z.mul (Z.of_int allocated) (Z.of_int 257)) consumed_storage in
        map_res aux op_internals >|? fun l ->
        let l_gas, l_storage = List.split l in
        let gas = List.fold_left Z.add consumed_gas l_gas in
        let storage = List.fold_left Z.add consumed_storage l_storage in
        (gas, storage)
      | Some { manager_meta_operation_result = Some op_res ; _ } ->
        Dune_utils.node_errors_to_string op_res.meta_op_status op_res.meta_op_errors in
    aux res >>? fun (gas_limit, storage_limit) ->
    let op = update_op_more_limits ~gas_limit ~storage_limit op in
    check_original_limits ~gas_limit ~storage_limit op >|? fun op ->
    iter_compute_fees ~first op 1

  let forge_auto_fees ~base ~head ~chain_id ops =
    run_operation ~base ~head ~chain_id (get_ops_more ops) >>=? fun ops_res ->
    let l = List.combine ops ops_res in
    return (mapi_res (fun i (op, res) ->
        update_gas_storage_fee ~first:(i=0) op res) l) >>=? fun ops ->
    return (map_res Forge.forge_operation (get_ops_more ops)) >>|? fun ops_bytes ->
    List.map2 update_op_more_bytes ops_bytes ops

  let prepare_ops ~base ~src ops =
    get_account_info ~base src >>=? fun {node_ai_counter; _} ->
    let counter = Misc.unopt Z.zero node_ai_counter in
    let ops = update_op_more_counters counter ops in
    get_constants ~base () >>=?
    fun { hard_gas_limit_per_operation; hard_storage_limit_per_operation ; _ } ->
    let ops = List.map
        (update_op_more_limits ~fee:0L
           ~gas_limit:hard_gas_limit_per_operation
           ~storage_limit:hard_storage_limit_per_operation) ops in
    return (map_res (fun {op; _} -> Forge.forge_operation op) ops) >>|? fun ops_bytes ->
    List.map2 update_op_more_bytes ops_bytes ops

  let sign_bytes_target options = function
    | None -> return (Ok None)
    | Some (pkh, sign) ->
      let bytes = Forge.forge_manage_account_options options in
      sign bytes >>|? fun s ->
      Some (pkh, Some (Crypto.(Base58.simple_encode Prefix.ed25519_signature s)))

  let make_reveal ~base ~get_pk src =
    get_manager_key ~base src >>=? function
    | Some _ -> return (Ok None)
    | None -> get_pk () >>|? fun pk ->
      Some (NReveal {
          node_rvl_src = src; node_rvl_pubkey = pk;
          node_rvl_fee = 0L; node_rvl_counter = Z.zero;
          node_rvl_gas_limit = Z.of_int 10000;
          node_rvl_storage_limit = Z.zero;
          node_rvl_metadata = None })

  let forge_manager_operations_base ~base ~get_pk ~src ops =
    let limits0 = List.map (fun {Metal_types.mo_info; _} ->
        mo_info.Metal_types.not_mi_fee, mo_info.Metal_types.not_mi_gas_limit,
        mo_info.Metal_types.not_mi_storage_limit) ops in
    To_dune.dune_ops ~sign_target:sign_bytes_target ~src ops >>=? fun ops ->
    make_reveal ~base ~get_pk src >>|?
    Misc.unoptf (ops, limits0)
      (fun rvl -> rvl :: ops, (None, Some (Z.of_int 10000), Some Z.zero) :: limits0)
    >>=? fun (ops, limits0) ->
    let ops = List.map2 make_op_more ops limits0 in
    prepare_ops ~base ~src ops >>=? fun ops ->
    get_header ~base () >>=? fun header ->
    let head = Misc.unopt_exn header.header_hash in
    let chain_id = Misc.unopt_exn header.header_network in
    forge_auto_fees ~base ~head ~chain_id ops >>|? fun ops ->
    ops, head

  let forge_manager_operations ~base ~get_pk ~src ops =
    forge_manager_operations_base ~base ~get_pk ~src ops >>=? fun (ops, head) ->
    let bytes = Forge.forge_operations_base head (get_ops_bytes_more ops) in
    compare_with_remote ~base ~head (get_ops_more ops) bytes >>|? fun bytes ->
    bytes, (get_ops_more ops)

  let forge_empty_account ~base ~get_pk ~src dst =
    if String.length dst < 2 || String.sub dst 0 2 <> "dn" then
      return @@ Error (Str_err "Cannot empty an account to a KT1")
    else (
      get_account_info ~base src >>=? fun info ->
      let tr = {
        Metal_types.mo_info = {
          Metal_types.not_mi_fee = None; not_mi_gas_limit = None;
          not_mi_storage_limit = None; not_mi_msg = None};
        mo_det = Metal_types.TraDetails {
            Metal_types.trd_dst = dst; trd_amount = info.node_ai_balance;
            trd_parameters = None}
      } in
      forge_manager_operations_base ~base ~get_pk ~src [ tr ] >>=? fun (ops, head) ->
      let ops = get_ops_more ops in
      let fees, _, _ = Dune_utils.limits_of_operations ops in
      let fix_amount tr =
        {tr with node_tr_amount = Int64.(pred (sub tr.node_tr_amount fees))} in
      let ops = match ops with
        | [ rvl; NTransaction tr ] -> [rvl; NTransaction (fix_amount tr)]
        | [ NTransaction tr ] -> [ NTransaction (fix_amount tr) ]
        | l -> l in
      match Forge.forge_operations head ops with
      | Error e -> return @@ Error e
      | Ok bytes -> return (Ok (bytes, ops))
    )

end
