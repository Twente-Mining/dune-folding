(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Dune_types_min
open Binary_writer

module Watermark = struct
  let block = "\001"
  let endorsement = "\002"
  let generic = "\003"
end

let forge_love_expr e =
  let open Love_binary_processor.Serializer in
  match e with
  | LoveValue lv ->
     list [ uint8 0; elem (Value.serialize lv) ]
  | LoveType lt ->
    list [ uint8 1; elem (Type.serialize lt) ]

let forge_love_code c =
  let open Love_binary_processor.Serializer in
  match c with
  | LoveTopContract ltc ->
    list [ uint8 0; elem (Located.serialize_top_contract ltc) ]
  | LoveLiveContract llc ->
    list [ uint8 1; elem (Value.serialize_live_contract llc) ]
  | LoveFeeCode lfc ->
    list [ uint8 2; Value.serialize_fee_code lfc ]

let forge_dune_expr = function
  | LoveExpr e -> list [ uint8 1; forge_love_expr e ]

let forge_dune_code = function
  | LoveCode c -> list [ uint8 1; forge_love_code c ]

let forge_script_expr = function
  | Micheline sc -> elem (list [
      uint8 0;
      Micheline_binary_writer.forge_micheline sc ])
  | DuneExpr sc -> elem (list [ uint8 1; forge_dune_expr sc ])
  | DuneCode sc -> elem (list [ uint8 2; forge_dune_code sc ])

let forge_entrypoint = function
  | None | Some EPDefault -> uint8 0
  | Some EPRoot -> uint8 1
  | Some EPDo -> uint8 2
  | Some EPSet -> uint8 3
  | Some EPRemove -> uint8 4
  | Some EPNamed s ->
    list [ uint8 255; uint8 (String.length s); Bigstring.of_string s ]

let forge_script script =
  match script with
  | None -> uint8 0
  | Some {sc_code; sc_code_hash; sc_storage} ->
    let sc_code = match sc_code with
      | None -> None
      | Some code -> Some (Dune_encoding_min.Script.decode code) in
    let sc_storage = Dune_encoding_min.Script.decode sc_storage in
    match sc_code, sc_code_hash with
    | Some code, None ->
      list [ uint8 255; forge_script_expr code; forge_script_expr sc_storage ]
    | None, Some code_hash ->
      list [ uint8 1; script_expr_hash code_hash; forge_script_expr sc_storage ]
    | Some code, Some code_hash ->
      list [ uint8 2; forge_script_expr code; script_expr_hash code_hash;
             forge_script_expr sc_storage ]
    | None, None -> assert false

let forge_collect_call fee_gas k =
  opt (fun f -> list [n_zarith f; opt pk k]) fee_gas

let forge_transaction_options collect_fee_gas collect_pk entry params =
  match collect_fee_gas, collect_pk, entry, params with
  | None, None, None, None -> 108, bool false
  | None, None, e, Some (Micheline sc_m) ->
    108, list [ bool true; forge_entrypoint e; elem (
        Micheline_binary_writer.forge_micheline sc_m) ]
  | fee_gas, collect_pk, e, sc ->
    let param = list [ forge_entrypoint e; opt forge_script_expr sc ] in
    let collect_call = forge_collect_call fee_gas collect_pk in
    203, list [ param; collect_call ]

let forge_activation act = (* 41 bytes *)
  list [
    uint8 4;
    Crypto.Pkh.b58dec act.node_act_pkh;
    Hex.to_bigstring (`Hex act.node_act_secret) ]

let forge_proposals pr =
  let props = list @@
    List.map (Crypto.(Base58.simple_decode Prefix.protocol_hash))
      pr.node_prop_proposals in
  list [
    uint8 5;
    pkh pr.node_prop_src;
    int32 (Int32.to_int pr.node_prop_voting_period);
    elem props
  ]

let forge_ballot ba =
  list [
    uint8 6;
    pkh ba.node_ballot_src;
    int32 (Int32.to_int ba.node_ballot_voting_period);
    Crypto.(Base58.simple_decode Prefix.protocol_hash ba.node_ballot_proposal);
    match ba.node_ballot_vote with
    | Yay -> uint8 0
    | Nay -> uint8 1
    | Pass -> uint8 2 ]

let forge_manager_op src fee counter gas_limit storage_limit =
  list [
    source_manager src;
    n_int64 fee;
    n_zarith counter;
    n_zarith gas_limit;
    n_zarith storage_limit ]

let forge_reveal rvl =
  list [
    uint8 107;
    forge_manager_op rvl.node_rvl_src rvl.node_rvl_fee
      rvl.node_rvl_counter rvl.node_rvl_gas_limit rvl.node_rvl_storage_limit;
    pk rvl.node_rvl_pubkey ]

let forge_transaction tr =
  let params = match tr.node_tr_parameters with
    | None -> None
    | Some p -> Some (Dune_encoding_min.Script.decode p) in
  let entry = match tr.node_tr_entrypoint with
    | None -> None
    | Some e -> Some (Dune_encoding_min.Script.ep_of_string e) in
  let tag, options = forge_transaction_options
      tr.node_tr_collect_fee_gas tr.node_tr_collect_pk entry params in
    list [
      uint8 tag;
      forge_manager_op tr.node_tr_src tr.node_tr_fee tr.node_tr_counter
        tr.node_tr_gas_limit tr.node_tr_storage_limit;
      n_int64 tr.node_tr_amount;
      contract tr.node_tr_dst ;
      options ]

let forge_origination ori =
  list [
    uint8 109;
    forge_manager_op ori.node_or_src ori.node_or_fee
      ori.node_or_counter ori.node_or_gas_limit ori.node_or_storage_limit;
    n_int64 ori.node_or_balance;
    uint8 0 ;
    forge_script ori.node_or_script
  ]

let forge_delegation dlg =
  list [
    uint8 110;
    forge_manager_op dlg.node_del_src dlg.node_del_fee
      dlg.node_del_counter dlg.node_del_gas_limit dlg.node_del_storage_limit;
    opt pkh dlg.node_del_delegate
  ]

let forge_protocol_parameters p =
  list [
    opt uint8 p.proof_of_work_nonce_size_opt;
    opt uint8 p.nonce_length_opt;
    opt uint8 p.max_revelations_per_block_opt;
    opt int32 p.max_operation_data_length_opt;
    opt uint8 p.max_proposals_per_delegate_opt;
    opt uint8 p.preserved_cycles_opt;
    opt int32 p.blocks_per_cycle_opt;
    opt int32 p.blocks_per_commitment_opt;
    opt int32 p.blocks_per_roll_snapshot_opt;
    opt int32 p.blocks_per_voting_period_opt;
    opt (fun l -> elem (
        list (List.map (fun t -> int64 (Int64.of_int t)) l)))
      p.time_between_blocks_opt;
    opt int16 p.endorsers_per_block_opt;
    opt (fun i -> zarith (Z.of_int64 i)) p.hard_gas_limit_per_operation_opt;
    opt (fun i -> zarith (Z.of_int64 i)) p.hard_gas_limit_per_block_opt;
    opt int64 p.proof_of_work_threshold_opt;
    opt n_int64 p.tokens_per_roll_opt;
    opt int16 p.michelson_maximum_type_size_opt;
    opt n_int64 p.seed_nonce_revelation_tip_opt;
    opt int32 p.origination_size_opt;
    opt n_int64 p.block_security_deposit_opt;
    opt n_int64 p.endorsement_security_deposit_opt;
    opt n_int64 p.block_reward_opt;
    opt n_int64 p.endorsement_reward_opt;
    opt n_int64 p.cost_per_byte_opt;
    opt (fun i -> zarith @@ Z.of_int64 i) p.hard_storage_limit_per_operation_opt;
    opt (fun i -> zarith @@ Z.of_int64 i) p.hard_gas_limit_to_pay_fees_opt;
    opt int32 p.protocol_revision;
    opt int32 p.max_operation_ttl_opt;
    opt int32 p.initial_endorsers_opt;
    opt (fun i -> int64 (Int64.of_int i)) p.delay_per_missing_endorsement_opt;
    elem (list (List.map (fun s -> elem (Bigstring.of_string s)) p.protocol_actions));
    opt int32 p.frozen_account_cycles_opt;
    opt bool p.allow_collect_call_opt ]

let forge_dune_manager_op src fee counter gas_limit storage_limit l =
  list [
    uint8 201;
    forge_manager_op src fee counter gas_limit storage_limit;
    elem @@ list l
  ]

let forge_activate_protocol acp =
  forge_dune_manager_op acp.node_acp_src acp.node_acp_fee acp.node_acp_counter
    acp.node_acp_gas_limit acp.node_acp_storage_limit [
    uint8 0;
    int32 acp.node_acp_level;
    opt (fun p -> Crypto.(Base58.simple_decode Prefix.protocol_hash p))
      acp.node_acp_protocol;
    opt forge_protocol_parameters acp.node_acp_parameters
  ]

let forge_dune_manage_accounts macs =
  forge_dune_manager_op macs.node_macs_src macs.node_macs_fee
    macs.node_macs_counter macs.node_macs_gas_limit macs.node_macs_storage_limit [
    uint8 1;
    elem (Hex.to_bigstring macs.node_macs_bytes)
  ]

let forge_manage_account_options mao =
  let bs = Json_repr_bson.Json_encoding.construct
      Dune_encoding_min.Operation.mao_encoding mao in
  let b = Bigstring.of_bytes @@ Json_repr_bson.bson_to_bytes bs in
  elem b

let forge_dune_manage_account mac =
  forge_dune_manager_op mac.node_mac_src mac.node_mac_fee mac.node_mac_counter
    mac.node_mac_gas_limit mac.node_mac_storage_limit [
    uint8 2;
    opt (fun (h, sign) ->
        list [ pkh h;
                  opt signature sign ])
      mac.node_mac_target;
    elem (forge_manage_account_options mac.node_mac_options)
  ]

let forge_operation_exn = function
  | NActivation act -> forge_activation act
  | NProposals pr -> forge_proposals pr
  | NBallot ba -> forge_ballot ba
  | NReveal rvl -> forge_reveal rvl
  | NTransaction tr -> forge_transaction tr
  | NOrigination ori -> forge_origination ori
  | NDelegation dlg -> forge_delegation dlg
  | NActivate_protocol acp -> forge_activate_protocol acp
  | NManage_accounts macs -> forge_dune_manage_accounts macs
  | NManage_account macs -> forge_dune_manage_account macs
  | _ -> Bigstring.empty

let forge_operation op =
  try Ok (forge_operation_exn op)
  with exn -> Error (MRes.Str_err (Printexc.to_string exn))

let forge_operations_base block ops_bytes =
  list [ branch block; list ops_bytes ]

let forge_operations_exn block ops =
  forge_operations_base block @@ List.map forge_operation_exn ops

let forge_operations branch ops =
  try Ok (forge_operations_exn branch ops)
  with exn -> Error (MRes.Str_err (Printexc.to_string exn))

let to_hex b = Hex.(show (of_bigstring b))
let of_hex h = Hex.to_bigstring (`Hex h)

let sign_exn ?(watermark=Watermark.generic) ~sk bytes =
  let sk = Hacl.Sign.unsafe_sk_of_bytes sk in
  let msg = Crypto.Blake2b_32.hash_bytes [Bigstring.of_string watermark; bytes] in
  let signature = Bigstring.create 64 in
  Hacl.Sign.sign ~sk ~msg ~signature;
  signature

let sign ?watermark ~sk bytes =
  try Some (sign_exn ?watermark ~sk bytes)
  with _ -> None
