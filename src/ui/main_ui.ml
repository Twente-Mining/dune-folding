let () =
  EzXhr.init ();
  let path = Route.init () in
  Register.init ();
  let app = Vdata.init path in
  Route.route ~app path;
  Request.init (fun _ -> ())
