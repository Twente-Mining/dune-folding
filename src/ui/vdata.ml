open Js_of_ocaml.Js

class type error = object
  method code : int readonly_prop
  method content : js_string t optdef readonly_prop
end

class type donor = object
  method pkh : js_string t readonly_prop
  method name : js_string t readonly_prop
  method id : int readonly_prop
  method wus : int readonly_prop
  method credit : int readonly_prop
  method tsp : js_string t readonly_prop
end

include Vue_js.Make(struct

    class type data = object
      method currentPath : js_string t prop
      method pkh : js_string t prop
      method folding_name_ : js_string t prop
      method folding_id_ : js_string t prop
      method registerError : error t optdef prop
      method registering : bool t prop
      method registered : bool t prop
      method infoError : error t optdef prop
      method info : donor t optdef prop
      method donor_id_ : js_string t prop
    end

    type all = data
    let id = "app"
  end)

let init path =
  let data = object%js
    val mutable currentPath = string path
    val mutable pkh = string ""
    val mutable folding_name_ = string ""
    val mutable folding_id_ = string ""
    val mutable registerError = undefined
    val mutable registering = _false
    val mutable registered =  _false
    val mutable infoError = undefined
    val mutable info = undefined
    val mutable donor_id_ = string ""
  end in
  init ~data ()
