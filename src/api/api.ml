open EzAPIServerUtils

module MakeRegisterer(S: module type of Services)(H:module type of Handlers) = struct

  let register s h dir = register s (fun a _ b -> h a b) dir

  let register dir =
    dir
    |> register S.register_donor H.register_donor
    |> register S.donor_info H.donor_info

end

module R = MakeRegisterer(Services)(Handlers)

let services =
  empty |> R.register
