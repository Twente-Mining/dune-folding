open Data_types
open Metal_types
open MLet
open MAPI

module Node = Node.Make(EzCurl_lwt)

let (++) = Int32.add
let (--) = Int32.sub
let ( ** ) = Int32.mul

let baker = ref (None : string option)
let credit_reward = ref 1L
let credit_reward_percent = ref 0.
let period = ref 1l
let node = ref (EzAPI.TYPES.BASE "https://testnet-node.dunscan.io/")
let edsk = ref (None : string option)
let client_path = ref (None : string option)
let src = ref (None : string option)
let init_cycle = ref (None : string option)
let step = ref 3600
let dry_run = ref false

let speclist = [
  "--cycles", Arg.Int (fun i -> period := Int32.of_int i), "Time between payments (in cycles)";
  "--reward", Arg.Int (fun r -> credit_reward := Int64.of_int r), "Reward per credit in udun";
  "--reward-percent", Arg.String (fun r -> credit_reward_percent := float_of_string r /. 100.), "Reward percent (example 80)";
  "--node", Arg.String (fun s -> node := EzAPI.TYPES.BASE s), "Address of the node to inject transaction";
  "--baker", Arg.String (fun s -> baker := Some s), "Baker dn1 address";
  "--edsk", Arg.String (fun s -> edsk := Some s), "Unencrypted edsk";
  "--dune-client", Arg.String (fun s -> client_path := Some s), "Optional Dune client path";
  "--src", Arg.String (fun s -> src := Some s), "Optional source to use with dune-client";
  "--init", Arg.String (fun s -> init_cycle := Some s), "Initialize to cycle, ('current' for current cycle)";
  "--step", Arg.Set_int step, "Check every <step> seconds (default 3600)";
  "--dry-run", Arg.Set dry_run, "Don't send operation"
]

let get_accounts () =
  let> accounts = Dbr.get_accounts () in
  MLwt.map_res_s (fun (pkh, ({do_id; _} as account)) ->
      let@! donor = get1 Services.folding_host Services.team (string_of_int do_id) in
      pkh, account, donor) accounts

let account_credit pkh old_account new_account =
  let old_tsp = Misc_db.cal_of_str old_account.do_last in
  let new_tsp = Misc_db.cal_of_str new_account.do_last in
  if new_tsp > old_tsp then
    Some (pkh, new_tsp,
          Int64.(of_int (new_account.do_credit - old_account.do_credit)))
  else None

let mo_info = {not_mi_fee = None; not_mi_gas_limit = None;
               not_mi_storage_limit = None; not_mi_msg = None}

let make_operations ?base accounts =
  let l, total = List.fold_left (fun (ops, total) acc -> match acc with
      | None -> ops, total
      | Some (trd_dst, _, credit) ->
        let mo_det = TraDetails {
            trd_dst; trd_amount = credit;
            trd_parameters = None} in
        {mo_info; mo_det} :: ops, Int64.add total credit) ([], 0L) accounts in
  if l = [] then (Printf.printf "No account to reward\n%!"; l)
  else (
    Printf.printf "Inputs for rewards calculation :\n%!";
    Printf.printf "  Total donor credits : %Ld\n%!"  total;
    (match base with
     | Some b ->
       Printf.printf "  Total baking rewards : %Ld udun\n%!" b;
       Printf.printf "  Ratio distributed : %f\n%!" !credit_reward_percent
     | None ->
       Printf.printf "  udun per credit : %Ld\n%!" !credit_reward);
    Printf.printf "Reward transaction will contain: \n%!";
    List.map (function
        | {mo_det = TraDetails ({trd_amount; _} as tr); _} as op ->
          begin match base with
            | Some b ->
              let trd_amount2 =
                Int64.(div (of_float ((to_float (mul trd_amount b)) *.
                                      !credit_reward_percent)) total) in
              Printf.printf
                "  dst: %s -> amount: %Ld udun \
                 (= ratio_distributed * (credit=%Ld) * total_reward / total_credit)\n%!"
                tr.trd_dst trd_amount2 trd_amount;
              {op with mo_det = TraDetails {tr with trd_amount = trd_amount2} }
            | None ->
              let trd_amount2 = Int64.mul trd_amount !credit_reward in
              Printf.printf "  dst: %s -> amount: %Ld udun \
                             (= (udun per credit) * (credit=%Ld))\n%!" tr.trd_dst trd_amount2 trd_amount;
              {op with mo_det = TraDetails {tr with trd_amount = trd_amount} } end
        | op -> op) l)

let distribute_rewards_metal ops sk =
  let sk_b = Crypto.Sk.b58dec sk in
  let pk_b = Crypto.Sk.to_public_key sk_b in
  let pk = Crypto.Pk.b58enc pk_b in
  let src = Crypto.(Pkh.b58enc @@ Pk.hash pk_b) in
  let sign b = match Forge.sign ~sk:sk_b b with
    | None -> Lwt.return @@ (Error (MRes.Str_err "cannot sign"))
    | Some signature -> Lwt.return (Ok signature) in
  let@ bytes, _ops =
    Node.forge_manager_operations
      ~base:!node
      ~get_pk:(fun () -> Lwt.return (Ok pk)) ~src ops in
  let@ bytes = Node.sign_operation ~sign bytes in
  let@! bytes = Node.inject ~base:!node bytes in
  let op_hash = Crypto.Operation_hash.b58enc bytes in
  Printf.printf "Operation %s injected\n%!" op_hash;
  op_hash

let distribute_rewards_client ops client src =
  let total, ops_tup2 = List.fold_left (fun (tot, l) op ->
      match op with
      | {mo_det = TraDetails {trd_amount; trd_dst; _}; _} ->
        Int64.add trd_amount tot, (trd_dst, trd_amount) :: l
      | _ -> tot, l) (0L, []) ops in
  let total_dun = Int64.to_float total /. 1000000. in
  let s_batch = EzEncoding.construct Encoding.batch ops_tup2 in
  let dir = Filename.dirname client in
  let> oc = Lwt_io.open_file ~mode:Lwt_io.Output Filename.(concat dir "folding_reward.json") in
  let> () = Lwt_io.write oc s_batch in
  let> x = Lwt_unix.system (
      Printf.sprintf
        "%s dune batch transfer %f from %s to %s/folding_reward.json --burn-cap 0.257"
        client total_dun src dir) in
  match x with
  | Unix.WEXITED 0 -> Lwt.return (Ok "batch")
  | _ -> Lwt.return (Error (MRes.Str_err "operation failed with client"))

let distribute_rewards_base ?base cycle accounts =
  let ops = make_operations ?base accounts in
  if ops = [] then Lwt.return (Ok "empty")
  else
    let@ hash = match !edsk, !client_path, !src, !dry_run with
      | _, _, _, true -> Lwt.return (Error (MRes.Str_err "end of dry run"))
      | Some edsk, _, _, _ -> distribute_rewards_metal ops edsk
      | _, Some client, Some src, _ -> distribute_rewards_client ops client src
      | _ -> assert false in
    let total = List.fold_left (fun tot op ->
      match op with
      | {mo_det = TraDetails {trd_amount; trd_dst; _}; _} ->
        Int64.add trd_amount tot
      | _ -> tot) 0L ops in
    let>+ () = Dbw.register_distribution hash cycle ops total !credit_reward_percent in
    hash

let update_accounts cycle accounts =
  Lwt_list.iter_s (function
      | None -> Lwt.return_unit
      | Some (pkh, tsp, credit) ->
        Dbw.update_account pkh tsp credit cycle) accounts

let lookup_operation hash =
  let rec aux i =
    let> r = get1 !node Services.lookup_operation hash in
    match r with
    | Error _e ->
      Printf.printf "Lookup %d for operation %s failed\n%!" i hash;
      let> () = Lwt_unix.sleep 60. in
      aux (i+1)
    | Ok block_hash ->
      Printf.printf "Operation injected in block %s\n%!" block_hash;
      Lwt.return_unit in
  let> () = Lwt_unix.sleep 60. in
  aux 1

let distribute_rewards ?base cycle =
  let@ accounts = get_accounts () in
  let accounts = List.map (fun (pkh, o, n) -> account_credit pkh o n) accounts in
  let@ op_hash = distribute_rewards_base ?base cycle accounts in
  let@ () =
    if op_hash = "empty" || op_hash = "batch" then Lwt.return (Ok ())
    else let>+ () = lookup_operation op_hash in () in
  let>+ () = update_accounts cycle accounts in
  ()

let contract_rewards last_cycle current_cycle =
  match !baker with
  | None -> Lwt.return (Ok None)
  | Some baker ->
    let@! rewards = get1 !node Services.rewards baker in
    let rewards = List.fold_left (fun acc (cycle, rew) ->
        if cycle > last_cycle -- 1l && cycle < current_cycle then
          Int64.add acc rew
        else acc) 0L rewards in
    Printf.printf "Baker %s got %Ld udun rewards between cycles %ld and %ld\n%!"
      baker rewards last_cycle (current_cycle -- 1l);
    Some rewards

let init cycle =
  let@ current_cycle =
    if cycle = "current" then
      let@! (current_cycle, _) = get0 !node Services.head_level in
      current_cycle
    else Lwt.return (Ok (Int32.of_string cycle)) in
  let>+ () = Dbw.init_state current_cycle in ()

let loop () =
  Lwt_main.run (
    let@ () = match !init_cycle with None -> Lwt.return (Ok ()) | Some s -> init s in
    let rec aux () =
      let@ last_cycle = Dbr.get_last_cycle () in
      let@ (current_cycle, cycle_position) = get0 !node Services.head_level in
      if current_cycle > last_cycle ++ !period then
        let@ base = contract_rewards last_cycle current_cycle in
        let last_cycle_new = current_cycle -- 1l in
        let@ () = distribute_rewards ?base last_cycle_new in
        let@ () = Dbw.update_state last_cycle_new in
        aux ()
      else
        let@ (blocks_per_cycle, times) = get0 !node Services.constants in
        let t0 = float_of_string @@ List.hd times in
        let levels = Int32.to_float @@
          blocks_per_cycle ** !period
          -- (current_cycle -- last_cycle -- 1l) ** blocks_per_cycle
          -- cycle_position in
        let hours = (levels *. t0) /. 3600. in
        Printf.printf "Next distribution in %.1f hours\n%!" hours;
        let> () = Lwt_unix.sleep (min (levels *. t0) (float !step)) in
        aux ()
    in
    aux ()
  )

let () =
  Arg.parse speclist (fun _ -> ()) "Reward distributor";
  match !edsk, !src, !client_path, !dry_run with
  | Some _, _, _, _ | _, Some _, Some _, _ | _, _, _, true ->
    begin match loop () with
      | Error e -> MRes.print_error e
      | Ok () -> Printf.printf "reward distributor exited normally\n%!" end
  | _ ->
    Printf.printf "No secret key or complete dune-client information given\n%!";
    exit 2
