open MLet

let wrap_res = function
  | Ok x -> Ok x
  | Error (EzRequest_lwt.KnownError {error; _}) -> Error error
  | Error (EzRequest_lwt.UnknwownError {code; msg}) -> Error (MRes.Gen_err (code, msg))

let get0 ?msg ?params base service =
  let>! r = EzCurl_lwt.get0 ?msg ?params base service in wrap_res r
let get1 ?msg ?params base service arg =
  let>! r = EzCurl_lwt.get1 ?msg ?params base service arg in wrap_res r
