open Json_encoding
open Metal_types
open Data_types

let network = conv
    (function Mainnet -> "mainnet" | Testnet -> "testnet" | Devnet -> "devnet" | Custom s -> s)
    (function "mainnet" -> Mainnet | "testnet" -> Testnet | "devnet" -> Devnet | s -> Custom s)
    string

let ignore_enc encoding =
  conv (fun x -> x, ()) (fun (x, _) -> x) @@
  merge_objs encoding unit

let error = conv
    MRes.error_content
    (fun (code, content) -> MRes.Gen_err (code, content)) @@
  obj2 (req "code" int) (opt "content" string)

let api_result encoding = union [
    case (obj1 (req "error" error))
      (function Error e -> Some e | _ -> None)
      (fun e -> Error e);
    case encoding
      (function Ok x -> Some x | _ -> None)
      (fun x -> Ok x) ]

let donor_encoding =
  ignore_enc @@ conv
    (fun {do_name; do_id; do_wus; do_credit; do_last}
      -> (do_name, Some do_id, None, do_wus, do_credit, Some (Some do_last)))
    (fun (do_name, id, team, do_wus, do_credit, do_last)
      -> let do_id = match id, team with
          | Some do_id, None | None, Some do_id -> do_id
          | _ -> Printf.eprintf "cannot find id or team in donor"; assert false in
        let do_last = Misc.unopt "" @@ Misc.unopt None do_last in
        {do_name; do_id; do_wus; do_credit; do_last}) @@
  obj6
    (req "name" string)
    (opt "id" int)
    (opt "team" int)
    (req "wus" int)
    (req "credit" int)
    (opt "last" (option string))

let team_search =
  ignore_enc (obj1 (dft "results" (list donor_encoding) []))

let dune_donor =
  obj2 (req "pkh" string) (req "donor" donor_encoding)

let lookup_operation = ignore_enc @@
  obj1 (req "block_hash" string)

let register_input = conv
    (fun {re_pkh; re_folding} -> (re_pkh, re_folding))
    (fun (re_pkh, re_folding) -> {re_pkh; re_folding}) @@
  obj2
    (req "pkh" string)
    (req "folding_id" string)

let constants = ignore_enc @@
  obj2
    (req "blocks_per_cycle" int32)
    (req "time_between_blocks" (list string))

let current_level = ignore_enc @@
  obj2 (req "cycle" int32) (req "cycle_position" int32)

let rewards = list @@ ignore_enc @@
  obj2 (req "cycle" int32) (req "rewards" EzEncoding.int64)

let batch = list @@
  obj2
    (req "address" string)
    (req "amount" EzEncoding.int64)
