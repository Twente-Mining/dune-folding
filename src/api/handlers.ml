open MLet
open Data_types
open MAPI

let return x = Lwt.bind x EzAPIServerUtils.return

let rec get_donor s =
  match int_of_string_opt s with
  | None ->
    let@ donors = get0 ~params:[Services.param_team_name, EzAPI.TYPES.S s]
        Services.folding_host Services.team_search in
    begin match donors with
      | [ r ] -> get_donor (string_of_int r.do_id)
      | _ -> Lwt.return (Error (MRes.Str_err "Donor not found")) end
  | Some _i ->
    let>! donor = get1 Services.folding_host Services.team s in donor

let register_donor _params {re_pkh; re_folding} = return @@
  if String.length re_pkh < 36 then
    Lwt.return @@ Error (MRes.Str_err "Wrong pkh format")
  else
    let@ donor = get_donor re_folding in
    Printf.printf "Found donor %s (id=%d) for %s\n%!" donor.do_name donor.do_id re_pkh;
    let prefix = String.sub re_pkh 0 8 in
    if not (Misc.contains_str donor.do_name prefix) then
      Lwt.return @@ Error (MRes.Str_err ("Donor username doesn't contain " ^ prefix))
    else (
      Printf.printf "Registering donor %d: %s\n%!" donor.do_id donor.do_name;
      Dbw.register_account re_pkh donor)

let donor_info (_params, id) () = return @@
  let@ (pkh, acc) = Dbr.get_account id in
  let@! donor = get1 Services.folding_host Services.team (string_of_int acc.do_id) in
  pkh, {donor with do_credit = donor.do_credit - acc.do_credit}
