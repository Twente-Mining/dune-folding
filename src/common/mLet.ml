(* Options *)
let (let*) o f = match o with None -> None | Some x -> f x
let (let*!) o f = match o with None -> None | Some x -> Some (f x)

(* Results *)
let (let|) r f = match r with Error _ as e -> e | Ok x -> f x
let (let|!) r f = match r with Error _ as e -> e | Ok x -> Ok (f x)

(* Lwt *)
let (let>) p f = Lwt.bind p f
let (let>!) p f = Lwt.map f p

(* Lwt results *)
let (let@) p f = Lwt.bind p (function Error _ as e -> Lwt.return e | Ok x -> f x)
let (let@!) p f = Lwt.map (function Error _ as e -> e | Ok x -> Ok (f x)) p
let (let>+) p f = Lwt.bind p (fun x -> Lwt.return @@ Ok (f x))
